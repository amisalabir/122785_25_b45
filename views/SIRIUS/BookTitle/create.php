

<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";


include("header.php");
?>


<div class="text-center">
    <h1>Book Title - Entry Form</h1>
    <div class="form-group form">
    <form action="store.php" method="post">
        <div>
        <h3>Enter Book Name:</h3>
        <input type="text" name="bookName">
        </div>

        <div>
        <h3>Enter Author Name:</h3>
        <input type="text" name="authorName">
        </div>
        <br><br>
        <input type="submit" class="btn btn-primary" value="ENTER">

    </form>
        </div>

</div>




<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



<?php include("footer.php");


