
<?php
require_once("../../../vendor/autoload.php");

$objSummaryofOrganization = new \App\SummaryofOrganization\SummaryofOrganization();
$allData = $objSummaryofOrganization->trashed();

use App\Message\Message;
use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            width:100%;
            padding:0px;
            margin:0px;
        }
        /* p {font-size: 16px;}
         .bg-1 {
             background-color: #1abc9c;
             color: #222222;
         }
         .bg-2 {
             background-color: #474e5d;
             color: #ffffff;
         }
         .bg-3 {
             background-color: #ffffff;
             color: #555555;
         }
         .bg-4 {
             background-color: #2f2f2f;
             color: #ffffff;
         }
         .container {
             padding: 45px 5%;
         }
         td{
             border: 0px;
         }
         table{
             width: 90%;
         }
         .msg{
             height: 40px;
         }
         h1{
             font-weight: bold;
         }
         ul, li, form{
             display: block;
         }
         .menu{
             float: left;
         }
         .search{
             float: right;
         }
         .navbar > .menu> .nav > li.active >a{
             color: #c7254e;
             background-color: #122b40;
             text-transform: uppercase;
             font-weight: bold;
         }
         .navbar > .menu> .nav > li.active >a:hover{
             color: #eea236;
             background-color: #454545;
         }*/
    </style>
</head>
<body class="bg-4">
<div class="container bg-1">
    <h1>Organization Information - Trashed List</h1>
    <nav class="navbar">
        <div class="menu">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li class="active"><a href="index.php">Active List</a></li>
                <li class="active"><a href="create.php">New Entry</a></li>
            </ul>
        </div>
    </nav>
    <div class="box"></div>
    <table class="table table-striped text-center">

        <tr>
            <th class="text-center">Serial Number</th>
            <th class="text-center"ID</th>
            <th class="text-center">Organization Name</th>
            <th class="text-center">Company's Summary</th>
            <th class="text-center">Action Butttons</th>
        </tr>
        <?php
        $serial=1;
        foreach ($allData as $oneData) {

            if($serial%2){
                $bgColor = "#1b6d85";
            }else{
                $bgColor = "#555555";
            }
            echo "
            <tr class=''>
                <td>$serial</td>
                <td>$oneData->id</td>
                <td>$oneData->comp_name</td>
                <td>$oneData->sum_of_org</td>
                <td>
                <a href='recover.php?id=$oneData->id' class='btn btn-success'>Recover</a>
                <a href='delete.php?id=$oneData->id' class='btn btn-danger'>Delete</a>
                </td>
            </tr>
        ";
            $serial++;
        }

        ?>
    </table>
</div>

</body>
</html>