<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;


if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();



?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            width:100%;
            padding:0px;
            margin:0px;
        }
        .bg-1 {
            background-color: #F8F8F8;
            color: black;
        }
        /* p {font-size: 16px;}
         .margin {margin-bottom: 45px;}
         .bg-1 {
             background-color: #1abc9c;
             color: #ffffff;
         }
         .bg-2 {
             background-color: #474e5d;
             color: #ffffff;
         }
         .bg-3 {
             background-color: #ffffff;
             color: #555555;
         }
         .bg-4 {
             background-color: #2f2f2f;
             color: #fff;
         }
         .container-fluid {
             padding-top: 70px;
             padding-bottom: 70px;
         }
         .navbar {
             padding-top: 15px;
             padding-bottom: 15px;
             border: 0;
             border-radius: 0;
             margin-bottom: 0;
             font-size: 12px;
             letter-spacing: 5px;
         }
         .navbar-nav  li a:hover {
             color: #1abc9c !important;
         }

         input{
             color: black;
         }*/
    </style>
</head>
<body><div class="container bg-1 text-center">
    <h1 style="color:#2f2f2f">Gender Information</h1>
    <h4 class="massage"><?php echo $msg;?></h4>
    <div class="form-group Form">
        <form action="store.php" method="post">
            <div>
                <h3>Enter Your Name: </h3>
                <input type="text" name="userName">
            </div>

            <div>
                <h3>Enter Your Gender: </h3>
                <input type="radio" name="gender" value="Male">Male
                <input type="radio" name="gender" value="Female">Female
            </div>
            <input type="submit" class="btn btn-primary" value="CREATE">
            <a href='index.php' class='btn btn-primary'>Index</a>
        </form>
    </div>
</div>
<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    jQuery(function($){
        $('.massage').fadeOut(500);
        $('.massage').fadeIn(500);
        $('.massage').fadeOut(500);
        $('.massage').fadeIn(500);
        $('.massage').fadeOut(500);
        $('.massage').fadeIn(500);
        $('.massage').fadeOut(500);
    })
</script>
</body>
</html>