<?php
require_once("../../../vendor/autoload.php");

$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$oneData = $objGender->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Single Book Information</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            width:100%;
            padding:0px;
            margin:0px;
        }
        /*  p {font-size: 16px;}
          .margin {margin-bottom: 45px;}
          .bg-1 {
              background-color: #1abc9c;
              color: #ffffff;
          }
          .bg-2 {
              background-color: #474e5d;
              color: #ffffff;
          }
          .bg-3 {
              background-color: #ffffff;
              color: #555555;
          }
          .bg-4 {
              background-color: #2f2f2f;
              color: #fff;
          }
          .container-fluid {
              padding-top: 70px;
              padding-bottom: 70px;
          }
          .navbar {
              padding-top: 15px;
              padding-bottom: 15px;
              border: 0;
              border-radius: 0;
              margin-bottom: 0;
              font-size: 12px;
              letter-spacing: 5px;
          }
          .navbar-nav  li a:hover {
              color: #1abc9c !important;
          }

          input{
              color: black;
          }
          td{
              padding: 10px 20px;
          }
      </style>
</head>
<body class="bg-4">
<?php
echo "<div class='container text-center'>
        <table class='table table-striped table-bordered text-center'>
            <tr>
                <td>ID:</td>
                <td>$oneData->id</td>
                <td></td>
        </tr>
        <tr>
            <td>Person Name: </td>
            <td> $oneData->user_name</td>
            <td></td>
            </tr>
            <tr>
            <td>Gender: </td>
            <td>$oneData->gender</td>
            <td><a href='index.php' class='btn btn-info'>Back To Active List</a></td>
        </tr>
        </table>
        </div>
    ";
?>
</body>
</html>
