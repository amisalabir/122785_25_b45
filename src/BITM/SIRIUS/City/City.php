<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 3:25 PM
 */

namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class City extends DB
{
    private $id;
    private $division_name;
    private $district_name;
    private $city_name;

    public function setData($allPostData=null){

        if(array_key_exists("id", $allPostData)){
            $this->id= $allPostData['id'];

        }
        if(array_key_exists("division", $allPostData)){
            $this->division_name= $allPostData['division'];

        }
        if(array_key_exists("district", $allPostData)){
            $this->district_name= $allPostData['division'];

        }
        if(array_key_exists("cityName", $allPostData)){
            $this->city_name= $allPostData['cityName'];
        }
    }


    public function store(){
        $arrayData = array($this->division_name, $this->district_name, $this->city_name);
        $query = 'INSERT INTO city_info (division, district, city) VALUES (?, ?, ?)';


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been inserted successfully");
        }else{
            Message::setMessage("Failed! Data has not been inserted");
        }

        Utility::redirect('create.php');
    }

    public function index(){
        $sql = "Select * from city_info where soft_delete='No'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function view(){
        $sql = "Select * from city_info where id=".$this->id;
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


        Utility::redirect('index.php');
    }

    public function trashed(){
        $sql = "Select * from city_info where soft_delete='yes'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }



    public function update(){
        $arrayData = array($this->division_name, $this->district_name, $this->city_name);
        $query = 'UPDATE city_info SET division=?, district=?, city=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Updated successfully");
        }else{
            Message::setMessage("Failed! Data has not been Updated");
        }

        Utility::redirect('index.php');
    }




    public function trash(){
        $arrayData = array("Yes");
        $query = 'UPDATE city_info SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Trashed successfully");
        }else{
            Message::setMessage("Failed! Data has not been Trashed");
        }

        Utility::redirect('trashed.php');
    }


    public function recover(){
        $arrayData = array("No");
        $query = 'UPDATE city_info SET soft_delete=? WHERE id='.$this->id;


        $STH = $this->dbh->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
            Message::setMessage("Success! Data has been Recovered");
        }else{
            Message::setMessage("Failed! Data has not been Recovered");
        }

        Utility::redirect('index.php');
    }



    public function delete(){
        $sql = "DELETE from city_info where id=".$this->id;

        $result = $this->dbh->exec($sql);

        if($result){
            Message::setMessage("Success! Data has been Deleted");
        }else{
            Message::setMessage("Failed! Data has not been Deleted");
        }


        Utility::redirect('index.php');
    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city_info  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }





    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city_info  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }





    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byDivision']) && isset($requestArray['byDistrict']) )  $sql = "SELECT * FROM `city_info` WHERE `soft_delete` ='No' AND (`division` LIKE '%".$requestArray['search']."%' OR `district` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byDivision']) && !isset($requestArray['byDistrict']) ) $sql = "SELECT * FROM `city_info` WHERE `soft_delete` ='No' AND `division` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byDivision']) && isset($requestArray['byDistrict']) )  $sql = "SELECT * FROM `city_info` WHERE `soft_delete` ='No' AND `district` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->division);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->division);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->district);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->district);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }

    public function trashMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  city_info SET soft_delete='Yes' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  city_info SET soft_delete='No' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($markArray){


        foreach($markArray as $id){

            $sql = "Delete from city_info  WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from city_info  WHERE id=".$id;


            $STH = $this->dbh->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }


}