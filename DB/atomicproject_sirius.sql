-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2017 at 07:08 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicproject_sirius`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_info`
--

CREATE TABLE `birth_info` (
  `id` int(11) NOT NULL,
  `user_name` varchar(55) NOT NULL,
  `birth_date` date NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_info`
--

INSERT INTO `birth_info` (`id`, `user_name`, `birth_date`, `soft_delete`) VALUES
(1, 'Durjoy', '1997-09-29', 'No'),
(2, 'Ashok', '2017-01-27', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(55) NOT NULL,
  `author_name` varchar(88) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(4, 'A Brief History Of Time', 'Stephen Hawking', 'Yes'),
(5, 'You Can Win', 'Shiv Khera', 'Yes'),
(6, 'Misir Ali', 'Humayun Ahmed', 'Yes'),
(7, 'C Pragramming', 'Kamruzzaman Niton', 'No'),
(8, 'Programming With C', 'Schaums', 'No'),
(9, 'Mohakasher sob Kotha', 'Farsim Mannan Mohammadi', 'No'),
(10, 'Mathematical Astronomy', 'Abu Saleh Mohammad Nuruzzaman', 'No'),
(11, 'Shesher Kobita', 'Rabindranath Thagor', 'No'),
(12, 'Gitanjali', 'Rabindranath Thagor', 'No'),
(13, 'Debdash', 'Shorodchandra Chatterjee', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `city_info`
--

CREATE TABLE `city_info` (
  `id` int(11) NOT NULL,
  `division` varchar(55) NOT NULL,
  `district` varchar(55) NOT NULL,
  `city` varchar(55) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_info`
--

INSERT INTO `city_info` (`id`, `division`, `district`, `city`, `soft_delete`) VALUES
(1, 'Dha', 'Dha', 'ffffff', 'Yes'),
(3, 'Chittagong', 'Chittagong', 'Feni', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email_info`
--

CREATE TABLE `email_info` (
  `id` int(11) NOT NULL,
  `user_name` varchar(55) NOT NULL,
  `email_add` varchar(77) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_info`
--

INSERT INTO `email_info` (`id`, `user_name`, `email_add`, `soft_delete`) VALUES
(1, 'Akash', 'durjoykash@yahoo.com', 'No'),
(2, 'durjoy', 'durjoyb97@gmail.com', 'Yes'),
(3, 'Durjoy', 'bdurjoy@rocketmail.com', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `gender_info`
--

CREATE TABLE `gender_info` (
  `id` int(11) NOT NULL,
  `user_name` varchar(55) NOT NULL,
  `gender` varchar(22) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender_info`
--

INSERT INTO `gender_info` (`id`, `user_name`, `gender`, `soft_delete`) VALUES
(1, 'Durjoy', 'Male', 'Yes'),
(2, 'Sanchita', 'Female', 'No'),
(3, 'Durjoy', 'Male', 'No'),
(4, 'Arati', 'Female', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobby_info`
--

CREATE TABLE `hobby_info` (
  `id` int(11) NOT NULL,
  `user_name` varchar(33) NOT NULL,
  `hobbies` varchar(99) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby_info`
--

INSERT INTO `hobby_info` (`id`, `user_name`, `hobbies`, `soft_delete`) VALUES
(1, 'Durjoy', 'gardening  programming  sports ', 'No'),
(2, 'Akash', ' music programming reading sports ', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization_info`
--

CREATE TABLE `organization_info` (
  `id` int(11) NOT NULL,
  `comp_name` varchar(55) NOT NULL,
  `sum_of_org` varchar(500) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization_info`
--

INSERT INTO `organization_info` (`id`, `comp_name`, `sum_of_org`, `soft_delete`) VALUES
(1, 'Alto Management', 'This is an international organization.', 'No'),
(2, 'Codebar', 'This Is an It Giant', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `user_name` varchar(55) NOT NULL,
  `profile_pic_link` varchar(77) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `user_name`, `profile_pic_link`, `soft_delete`) VALUES
(4, 'gggg', '1486118472computer.jpg', 'No'),
(7, 'National Flag', '1486120922grunge_flag_of_bangladesh_by_al_zoro-d4q44gd.jpg', 'No'),
(8, 'ffffffffffff', '1486316924excel.png', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_info`
--
ALTER TABLE `birth_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_info`
--
ALTER TABLE `city_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_info`
--
ALTER TABLE `email_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender_info`
--
ALTER TABLE `gender_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby_info`
--
ALTER TABLE `hobby_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_info`
--
ALTER TABLE `organization_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_info`
--
ALTER TABLE `birth_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `city_info`
--
ALTER TABLE `city_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `email_info`
--
ALTER TABLE `email_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gender_info`
--
ALTER TABLE `gender_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hobby_info`
--
ALTER TABLE `hobby_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `organization_info`
--
ALTER TABLE `organization_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
